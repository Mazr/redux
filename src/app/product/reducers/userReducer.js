const initState = {
  userProfile: {}
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_USER_PROFILE':
      return {
        ...state,
        userProfile: action.payload
      };

    default:
      return state
  }
};
