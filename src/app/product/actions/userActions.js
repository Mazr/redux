export const getUserProfile = (id) => (dispatch) => {
  fetchData(id).then(result => {
    dispatch({
      type: 'GET_USER_PROFILE',
      payload: result
    });
  })

};

async function fetchData(id) {
  try {
    const result = await fetch('http://localhost:8080/api/user-profiles/' + id);
    const data = await result.json();
    return data;
  } catch (e) {
    console.error(e);
  }


}