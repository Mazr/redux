import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {getUserProfile} from "../actions/userActions";


class UserProfile extends Component {

  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    let url = this.props.location.pathname;
    const startIndex = url.lastIndexOf('\/') + 1;
    const id = url.substring(startIndex, url.length);
    this.props.getUserProfile(id);
  }

  render() {
    const {name, gender, description} = this.props.userProfile;
    return (
      <div>
        <h1>User Profile</h1>
        <h3>User Name : {name}</h3>
        <h3>Gender : {gender}</h3>
        <h3>Description : {description}</h3>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userProfile: state.user.userProfile
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getUserProfile
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);