import React, {Component} from 'react';
import {connect} from "react-redux";
import {getProductDetails} from '../actions/productActions';
import Details from "../components/Details";
import {bindActionCreators} from "redux";

class ProductDetails extends Component {
  componentDidMount() {
    this.props.getProductDetails();
  }

  render() {
    const {title, userId, id} = this.props.productDetails;

    return (
      <div className="">
        <h3>Product Details Page</h3>
        <Details title={title} userId={userId} id={id}/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  productDetails: state.product.productDetails
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getProductDetails
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
