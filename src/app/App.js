import React from 'react';
import ProductDetails from "./product/pages/ProductDetails";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./home/Home";
import UserProfile from "./product/pages/UserProfile";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/product-details" component={ProductDetails}/>
          <Route path="/user-profiles/1" component={UserProfile}/>
          <Route path="/user-profiles/2" component={UserProfile}/>
          <Route path="/" component={Home}/>
        </Switch>
      </Router>
    </div>
  );
};

export default App;